package com.melvin.spring.batch.demo4.Repo;

import com.melvin.spring.batch.demo4.Model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, String> {
}
