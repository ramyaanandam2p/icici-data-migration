package com.melvin.spring.batch.demo4.Model;

import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import static javax.persistence.GenerationType.IDENTITY;

@Data
@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private int CUST_ID;
    private String FIRSTNAME;
    private String LASTNAME;
    private String MOBILENO;
    private String EMAILID;
    private String DOB;
    private String IDTYPE;
    private String IDNO;
    private String BANK_CUST_ID;
    private String BANK_BAY_ID;
    private String BANK_RELATIONS;
    private String KYC_STATUS;
    private String KYC_VALIDITY;
    private String KYC_UPDATED;
    private String KYC_FROM;
    private String KYC_FETCH_STATUS;
    private String KYC_SYNC_TIME;
    private String KYC_SYNC_TRYCOUNT;
    private String KYC_FETCH_TIME;
    private String KYC_FETCH_COUNT;
    private String KYC_SYNC_STATUS;
    private String TOKEN;
    private String TOKEN_TYPE;
    private String TOKEN_CREATED;
    private String STATUS;
    private String OTP_STATUS;
    private String OTP_REFNUMBER;
    private String CREATED_DT;
    private String UPDATED_DT;
    private String CREATED_BY;
    private String UPDATED_BY;
}
