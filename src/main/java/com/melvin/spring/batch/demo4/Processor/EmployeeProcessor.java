//package com.melvin.spring.batch.demo4.Processor;
//
//import com.melvin.spring.batch.demo4.DTO.EmployeeDTO;
//import com.melvin.spring.batch.demo4.Model.Employee;
//import org.springframework.batch.item.ItemProcessor;
//import org.springframework.stereotype.Component;
//
//@Component
//public class EmployeeProcessor implements ItemProcessor<EmployeeDTO, Employee> {
//
//    @Override
//    public Employee process(EmployeeDTO employeeDTO) throws Exception {
//        Employee employee = new Employee();
//        employee.setFIRSTNAME(employeeDTO.getFIRSTNAME());
//        employee.setLASTNAME(employeeDTO.getLASTNAME());
//        employee.setMOBILENO(employeeDTO.getMOBILENO());
//        employee.setEMAILID(employeeDTO.getEMAILID());
//        employee.setDOB(employeeDTO.getDOB());
//        employee.setIDTYPE(employeeDTO.getIDTYPE());
//        employee.setIDNO(employeeDTO.getIDNO());
//        employee.setBANK_CUST_ID(employeeDTO.getBANK_CUST_ID());
//        employee.setBANK_BAY_ID(employeeDTO.getBANK_BAY_ID());
//        employee.setBANK_RELATIONS(employeeDTO.getBANK_RELATIONS());
//        employee.setKYC_STATUS(employeeDTO.getKYC_STATUS());
//        employee.setKYC_VALIDITY(employeeDTO.getKYC_VALIDITY());
//        employee.setKYC_UPDATED(employeeDTO.getKYC_UPDATED());
//        employee.setKYC_FROM(employeeDTO.getKYC_FROM());
//        employee.setKYC_FETCH_STATUS(employeeDTO.getKYC_FETCH_STATUS());
//        employee.setKYC_SYNC_TIME(employeeDTO.getKYC_SYNC_TIME());
//        employee.setKYC_SYNC_TRYCOUNT(employeeDTO.getKYC_SYNC_TRYCOUNT());
//        employee.setKYC_FETCH_TIME(employeeDTO.getKYC_FETCH_TIME());
//        employee.setKYC_FETCH_COUNT(employeeDTO.getKYC_FETCH_COUNT());
//        employee.setKYC_SYNC_STATUS(employeeDTO.getKYC_SYNC_STATUS());
//        employee.setTOKEN(employeeDTO.getTOKEN());
//        employee.setTOKEN_TYPE(employeeDTO.getTOKEN_TYPE());
//        employee.setTOKEN_CREATED(employeeDTO.getTOKEN_CREATED());
//        employee.setSTATUS(employeeDTO.getSTATUS());
//        employee.setOTP_STATUS(employeeDTO.getOTP_STATUS());
//        employee.setOTP_REFNUMBER(employeeDTO.getOTP_REFNUMBER());
//        employee.setCREATED_DT(employeeDTO.getCREATED_DT());
//        employee.setUPDATED_DT(employeeDTO.getUPDATED_DT());
//        employee.setCREATED_BY(employeeDTO.getCREATED_BY());
//        employee.setUPDATED_BY(employeeDTO.getUPDATED_BY());
//        System.out.println("inside processor " + employee.toString());
//        return employee;
//    }
//}