package com.melvin.spring.batch.demo4.Job;

import com.melvin.spring.batch.demo4.Model.Employee;
import com.melvin.spring.batch.demo4.Repo.EmployeeRepo;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.partition.support.MultiResourcePartitioner;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.sql.DataSource;
import java.io.IOException;
import java.net.MalformedURLException;

@Configuration
@EnableBatchProcessing
public class Demo4 {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
//    @Autowired
//    private EmployeeProcessor employeeProcessor;
    @Autowired
    private DataSource dataSource;
    @Autowired
    EmployeeRepo employeeRepo;

    @Bean("partitioner")
    @StepScope
    public Partitioner partitioner() {
        MultiResourcePartitioner partitioner = new MultiResourcePartitioner();
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource[] resources = null;
        try {
            resources = resolver.getResources("employees/*.csv");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        partitioner.setResources(resources);
        partitioner.partition(10);
        return partitioner;
    }

//    @Bean
//    @StepScope
//    Resource inputFileResource(@Value("#{jobParameters[fileName]}") final String fileName) throws Exception {
//        return new ClassPathResource(fileName);
//    }

    @Autowired
    private FlatFileItemReader <Employee> employeeReader;
    @Bean
    @StepScope
    @Qualifier("employeeReader")
    @DependsOn("partitioner")
    public FlatFileItemReader<Employee> employeeReader(@Value("#{stepExecutionContext['fileName']}") String filename)
            throws MalformedURLException {
        return new FlatFileItemReaderBuilder<Employee>().name("employeeReader")
                .delimited()
                .names(new String[] { "FIRSTNAME","LASTNAME","MOBILENO","EMAILID","DOB","IDTYPE","IDNO","BANK_CUST_ID","BANK_BAY_ID","BANK_RELATIONS","KYC_STATUS","KYC_VALIDITY","KYC_UPDATED","KYC_FROM","KYC_FETCH_STATUS","KYC_SYNC_TIME","KYC_SYNC_TRYCOUNT","KYC_FETCH_TIME","KYC_FETCH_COUNT","KYC_SYNC_STATUS","TOKEN","TOKEN_TYPE","TOKEN_CREATED","STATUS","OTP_STATUS","OTP_REFNUMBER","CREATED_DT","UPDATED_DT","CREATED_BY","UPDATED_BY" })
                .fieldSetMapper(new BeanWrapperFieldSetMapper<Employee>() {
                    {
                        setTargetType(Employee.class);
                    }
                })
                .resource(new UrlResource(filename))
                .build();
    }

    @Bean
    public JdbcBatchItemWriter<Employee> employeeDBWriterDefault() {
        JdbcBatchItemWriter<Employee> itemWriter = new JdbcBatchItemWriter<Employee>();
        itemWriter.setDataSource(dataSource);
        itemWriter.setSql("insert into employee (FIRSTNAME, LASTNAME, MOBILENO, EMAILID, DOB, IDTYPE, IDNO, BANK_CUST_ID, BANK_BAY_ID, BANK_RELATIONS, KYC_STATUS, KYC_VALIDITY, KYC_UPDATED, KYC_FROM, KYC_FETCH_STATUS, KYC_SYNC_TIME, KYC_SYNC_TRYCOUNT, KYC_FETCH_TIME, KYC_FETCH_COUNT, KYC_SYNC_STATUS, TOKEN, TOKEN_TYPE, TOKEN_CREATED, STATUS, OTP_STATUS, OTP_REFNUMBER, CREATED_DT, UPDATED_DT, CREATED_BY, UPDATED_BY) values (:FIRSTNAME, :LASTNAME, :MOBILENO, :EMAILID, :DOB, :IDTYPE, :IDNO, :BANK_CUST_ID, :BANK_BAY_ID, :BANK_RELATIONS, :KYC_STATUS, :KYC_VALIDITY, :KYC_UPDATED, :KYC_FROM, :KYC_FETCH_STATUS, :KYC_SYNC_TIME, :KYC_SYNC_TRYCOUNT, :KYC_FETCH_TIME, :KYC_FETCH_COUNT, :KYC_SYNC_STATUS, :TOKEN, :TOKEN_TYPE, :TOKEN_CREATED, :STATUS, :OTP_STATUS, :OTP_REFNUMBER, :CREATED_DT, :UPDATED_DT, :CREATED_BY, :UPDATED_BY)");
        itemWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Employee>());
        return itemWriter;
    }

    @Bean
    public Step step1Demo4(){
        return stepBuilderFactory.get("step1Demo4")
                .<Employee, Employee>chunk(10000)
                .reader(employeeReader)
//              .processor(employeeProcessor)
                .writer(employeeDBWriterDefault())
                .build();
    }

    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setMaxPoolSize(10);
        taskExecutor.setCorePoolSize(10);
        taskExecutor.setQueueCapacity(10);
        taskExecutor.afterPropertiesSet();
        return taskExecutor;
    }

    @Bean
    @Qualifier("masterStep")
    public Step masterStep() {
        return stepBuilderFactory.get("masterStep")
                .partitioner("step1Demo4", partitioner())
                .step(step1Demo4())
                .taskExecutor(taskExecutor())
                .build();
    }

    @Bean
    public Job demo4Job() throws Exception {
        return this.jobBuilderFactory.get("demo4")
                .incrementer(new RunIdIncrementer())
                .flow(masterStep())
                .end()
                .build();
    }
}
