package com.example.demo.controller;


import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Dao.MicroService;
import com.example.demo.entity.ICICIRep;
import com.example.demo.entity.Transaction;
import com.example.demo.entity.Wallet;
import com.example.demo.serviceimpl.TransactionRep;





@RestController
@RequestMapping("/get")
public class MicroController {
	
	@Autowired
	MicroService service;
	
	@Autowired
	private ICICIRep icicirep;
	
	@Autowired
	private TransactionRep transactionRep;

	 @GetMapping("/String/findById/{pkey}")
	 public List<Object> findById(@PathVariable long pkey) {
		 		 		
	List<Wallet> wallets=	icicirep.findById(pkey);
	
	String entityId=wallets.get(0).getEntity_id();

	
	double balances=wallets.get(0).getBalance();
	double balance=	transaction(entityId,balances);
	
	List<Object> wallet=new ArrayList<>();
	
	wallet.add(balance);
	
	Wallet wallet1=new Wallet();
	
	wallet1.setBalance(balance);
	
	icicirep.save(wallet1);
	
	wallet.add(entityId);
	
	return wallet;
		 
	 }

	private double transaction(String entityId, double balances) {
		List<Transaction> transaction=	transactionRep.findById(1);
	
		double amount=0;

		if(entityId.equalsIgnoreCase(transaction.get(0).getEntity_id())) {
			if(transaction.get(0).getCredit_debit().equalsIgnoreCase("credit")) {
	 amount=balances+transaction.get(0).getAmount();
			}
			else {
				 amount=balances-(transaction.get(0).getAmount());
	
			}
			
		}
		return amount;
	}
}
