package com.example.demo.controller;


import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.Dao.DataBaseManager;
import com.example.demo.entity.GenericEntity;
import com.example.demo.serviceimpl.GenericFileReader;
import com.opencsv.CSVWriter;

@Controller

public class GenericUPloader {
	
		  
	@PostMapping(path = "fetchdata")
	public ResponseEntity<String>  GetEmployeeData(@RequestBody GenericEntity FDE) throws IOException
	{
		
			if (FDE != null)
			{
				String fileName= FDE.getFile_name();
				int splitSize = FDE.getSplit_size();
				
			GenericFileReader gfr= new GenericFileReader();
			
			gfr.ProcessInputs(fileName, splitSize);
		 			return ResponseEntity.ok("Success");	

			
			}
			return ResponseEntity.ok("Failure");
		} 
	
	
	@PostMapping(path = "dataWriter")
	public ResponseEntity<String>  dataWriter() throws IOException 
	{		
		List<String[]>  allData= new ArrayList<String[]>();

		  for(int index=1;index<=200000;index++) 
		  {
			  String[] array = new String[] {String.valueOf(index),"ICICI_SIM_DATA_" + index};
			  allData.add(array);
		  }
		  
		  CSVWriter writer = new CSVWriter(new FileWriter("C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\icici.csv"));
	      writer.writeAll(allData);
	      writer.flush();
		  
		 return ResponseEntity.ok("Success");		      
	  }

}
