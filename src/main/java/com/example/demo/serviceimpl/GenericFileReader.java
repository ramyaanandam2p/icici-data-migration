package com.example.demo.serviceimpl;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Index;

import com.example.demo.Dao.DataBaseManager;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

public class GenericFileReader {
	

		public void ProcessInputs(String fileName,int splitSize) throws IOException{
			
			List<Object> str=new ArrayList<>();
			
			str.add(splitSize);
			
			str.add(fileName);
			
			System.out.println("File Name"+fileName);
			
			System.out.println("Split Size"+splitSize);

			   CSVReader reader = new CSVReader(new FileReader(fileName));
			      //Reading the contents of the  file
			   
			     int iIndex = 1;
			     int iSplitStart = 0;
			     List<String[]> allData =  reader.readAll();
			     int iRecordsCount = allData.size();
			     
			   while(true) 
			   {
				   if((iSplitStart) >= iRecordsCount)
				   {
					   iIndex--;
					   break;
				   }
				   else
				   {
					   if((iSplitStart + splitSize) < iRecordsCount)
					   {
						  int endIndex = iSplitStart + splitSize;
						 List<String[]>    allData1= allData.subList(iSplitStart, endIndex);
							   iSplitStart += splitSize;
							   CSVWriter writer = new CSVWriter(new FileWriter(fileName.substring(0, fileName.length()-4)+"_" + iIndex + ".csv"));
							      writer.writeAll(allData1);
							      writer.close();							      
					   }
					   else
					   {
						   List<String[]>  allData2= allData.subList(iSplitStart, iRecordsCount);
							   iSplitStart =  iRecordsCount;
							   CSVWriter writer = new CSVWriter(new FileWriter(fileName.substring(0, fileName.length()-4)+"_"+ iIndex + ".csv"));
							      writer.writeAll(allData2);
							      //writer.flush();	
							      writer.close();
					   }			
					   ++iIndex;
				   }					      
			   }
			   
			   DataBaseManager dbm = new DataBaseManager();
  
			   dbm.insert();			
		}
}
