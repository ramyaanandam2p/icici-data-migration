package com.example.demo.serviceimpl;

import java.util.List;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Transaction;

@Repository
public interface TransactionRep extends CrudRepository<Transaction, Long> {

	List<Transaction> findById(long pkey);

}
