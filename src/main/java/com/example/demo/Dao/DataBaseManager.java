package com.example.demo.entity;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.sql.DataSource;
import org.springframework.jdbc.core.RowMapper;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;


@SpringBootApplication
public class DataBaseManager implements CommandLineRunner
{

	private static JdbcTemplate m_jdbcT;
	private static DataBaseManager m_DBM = null;
	
	@Autowired
	public void SetJDBCTemplate(JdbcTemplate jdbcT) 
	{
		m_jdbcT = jdbcT;
	}
	
	public static DataBaseManager GetInstance()
	{
		if(m_DBM == null)
		{
			m_DBM = new DataBaseManager();
		}
		
		
		return m_DBM;
	}
	
	public void Execute1() throws IOException
	{
		m_jdbcT.execute("INSERT INTO fingpay.business_entity (pkey,firstName,lastName,contact_no,emailAddress,special_date,idType,idNumber,created,changed,creator,changer)\r\n"
				+ "SELECT  CUST_ID,FIRSTNAME,LASTNAME,MOBILENO,EMAILID,DOB,IDTYPE,IDNO,CREATED_DT,UPDATED_DT,CREATED_BY,UPDATED_BY FROM walletdb.cust_tbl");
		
		m_jdbcT.execute("INSERT INTO fingpay.channel (pkey,channel,description,maxBalance,minBalance)\r\n"
				+ "SELECT LIMIT_ID,CODE,NAME,MAX_BAL,MIN_BAL FROM walletdb.limits_tbl");
		
		m_jdbcT.execute("INSERT INTO fingpay.wallet (dailyCredit,dailyDebit,weeklyCredit,weeklyDebit,monthlyCredit,monthlyDebit,yearlyCredit,yearlyDebit,deleted,created,changed,creator,changer)\r\n"
				+ "SELECT DAILY_MAX_CREDIT,DAILY_MAX_DEBIT,WEEKLY_MAX_CREDIT,WEEKLY_MAX_DEBIT,MONTHLY_MAX_CREDIT, MONTHLY_MAX_DEBIT,YEARLY_MAX_CREDIT,YEARLY_MAX_DEBIT,STATUS,CREATED_DTUPDATED_DT,CREATED_BY,UPDATED_BY FROM walletdb.limits_tbl\r\n"
				+ "");
		
		m_jdbcT.execute("INSERT INTO fingpay.otp (pkey,entityId,mobileNo,encryptedOtp,attempts,otpType,created,changed,creator,changer)\r\n"
				+ "SELECT OTP_REQ_ID,WALLET_ID,MOBILENO,GENERATE_URN,VALIDATION_COUNT,PURPOSE,CREATED_DT,UPDATED_DT,CREATED_BY,UPDATED_BY FROM walletdb.otp_details_tbl\r\n"
				+ "\r\n"
				+ "");
		
		m_jdbcT.execute("INSERT INTO fingpay.wallet (pkey,business_entity_fkey,product_fkey,wallet_type_fkey,balance,created,changed,creator,changer)\r\n"
				+ "SELECT WALLET_ID,WALLET_ID,POUCH_CODE,POUCH_CODE,BALANCE,CREATED_DT,UPDATED_DT,CREATED_BY,UPDATED_BY FROM walletdb.pouch_tbl\r\n"
								+ "");
		
		m_jdbcT.execute("INSERT INTO fingpay.transaction (pkey,wallet,tx_ref,ext_txn_id,fromWallet,txnType,amount,fromWalletBalance,tx_status,created,changed,creator,changer)\r\n"
				+ "SELECT POUCH_TXN_ID,WALLET_ID,TXN_ID,RRN,CREDIT_DEBIT_FLAG,TXN_TYPE,AMOUNT,BALANCE,STATUS,CREATED_DT,UPDATED_DT,CREATED_BY,UPDATED_BY FROM walletdb.pouch_txn_tbl");
		
	}
	public void Execute() throws IOException
	{
		try{  
			  // Reading file and getting no. of files to be generated  
			  String inputfile = "C:\\Users\\RamyaAnanda\\Desktop\\Wallet.txt"; //  Source File Name.  
			  double nol = 500.0; //  No. of lines to be split and saved in each output file.  
			  File file = new File(inputfile);  
			  Scanner scanner = new Scanner(file);  
			  int count = 0;  
			  while (scanner.hasNextLine())   
			  {  
			   scanner.nextLine();  
			   count++;  
			  }  
			  System.out.println("Lines in the file: " + count);     // Displays no. of lines in the input file.  

			  double temp = (count/nol);  
			  int temp1=(int)temp;  
			  int nof=0;  
			  if(temp1==temp)  
			  {  
			   nof=temp1;  
			  }  
			  else  
			  {  
			   nof=temp1+1;  
			  }  
			  System.out.println("No. of files to be generated :"+nof); // Displays no. of files to be generated.  

			  //---------------------------------------------------------------------------------------------------------  

			  // Actual splitting of file into smaller files  

			  FileInputStream fstream = new FileInputStream(inputfile); DataInputStream in = new DataInputStream(fstream);  

			  BufferedReader br = new BufferedReader(new InputStreamReader(in)); String strLine;  

			  for (int j=1;j<=nof;j++)  
			  {  
			   FileWriter fstream1 = new FileWriter("C:\\Users\\RamyaAnanda\\Desktop\\Wallet_"+j+".txt");     // Destination File Location  
			   BufferedWriter out = new BufferedWriter(fstream1);   
			   for (int i=1;i<=nol;i++)  
			   {  
			    strLine = br.readLine();   
			    if (strLine!= null)  
			    {  
			     out.write(strLine);   
			     if(i!=nol)  
			     {  
			      out.newLine();  
			     }  
			    }  
			   }  
			   out.close();  
			  }  

			  in.close();  
			 }catch (Exception e)  
			 {  
			  System.err.println("Error: " + e.getMessage());  
			 }  

			}  

	
	
	public DataSource GetDataSource()
	{
		return m_jdbcT.getDataSource();
	}

	@Override
	public void run(String... args) throws Exception
	{
		// TODO Auto-generated method stub
		
	}

public void file() throws IOException {
	String [] fileNames= {"Wallet","Wallet1","Wallet2"};
	String content = null;
String contents="";
    for (String files : fileNames)
    {
    	
    	BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\RamyaAnanda\\Desktop\\Wallet.txt"));
    
	StringBuilder stringBuilder = new StringBuilder();
	String line = null;
	String ls = System.getProperty("line.separator");
	while ((line = reader.readLine()) != null) {
		stringBuilder.append(line);
		stringBuilder.append(ls);
	}
	// delete the last new line separator
	stringBuilder.deleteCharAt(stringBuilder.length() - 1);
	reader.close();

	 content = stringBuilder.toString();
    
	  contents+=content;
    }
	 Execution(contents);


}

private void Execution(String query) {
	   String[] arrOfStr = query.split(";");
       
       for (String a : arrOfStr)
       {
			m_jdbcT.execute(a);
       }
	}               
	
}

