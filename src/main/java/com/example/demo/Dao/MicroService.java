package com.example.demo.Dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication


public class MicroService implements CommandLineRunner{
	private static JdbcTemplate m_jdbcT;
	private static MicroService m_DBM = null;
	
	@Autowired
	public void SetJDBCTemplate(JdbcTemplate jdbcT) 
	{
		m_jdbcT = jdbcT;
	}
	
	public static MicroService GetInstance()
	{
		if(m_DBM == null)
		{
			m_DBM = new MicroService();
		}
		
		
		return m_DBM;
	}
	
	public void Execute()
	{
		
			m_jdbcT.execute("INSERT INTO"+" icici.customer"+
					"("+ "pkey,"+"entity_id,"+"title,"+"first_name,"+"last_name,"+"gender,"+"address,"+"address2,"+"city,"+
					"country,"+"state,"+"pincode,"+"country_of_issue,"+"description,"+"contact_no,"+"email,"+"kit"+")"+ "VALUES"+
							"("+"'1'"+","+"'RAMYA001'"+","+"'MR'"+","+"'AJAY'"+","+"'THAKUR'"+","+"'M'"+
					","+"'Ramchandra Nagar No 2 Chawl No,35 '"+","+"' ESIS Hospital Road Wagle Thane west'"+","+
							"'THANE'"+","+"'INDIA'"+","+"'MAHARASHTRA'"+","+"'400604'"+","+"'IND'"+","+"'ICICI'"+","+
					"'9022320390'"+","+"'athakur867@gmail.com'"+","+"'6785084'"+")");
		
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
}
