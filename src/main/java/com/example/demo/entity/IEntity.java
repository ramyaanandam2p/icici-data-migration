package com.example.demo.entity;

public interface IEntity {

	public Long getPkey();

    public void setPkey(Long pkey);

    public Boolean getDeleted();

    public void setDeleted(Boolean deleted);
}
