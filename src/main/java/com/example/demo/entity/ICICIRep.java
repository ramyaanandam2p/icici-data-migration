package com.example.demo.entity;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICICIRep extends CrudRepository<Wallet, Long> {

	List<Wallet> findById(long pkey);

}
