package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GenericEntity {
	
	@JsonProperty
	private String File_name;
	
	@JsonProperty
	private int split_size;

}
