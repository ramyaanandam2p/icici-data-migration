package com.example.demo.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "transaction")
@Getter
@Setter
public class Transaction {
	

	@Column
	private double amount;
	@Column
	private String entity_id;
	@Column
	private String description;
	@Column
	private String credit_debit;
	
}
